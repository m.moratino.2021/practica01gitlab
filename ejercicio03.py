

#(********)
#(* main *)
#(********)

if __name__ == "__main__":
    lista_entrada = ['A','B','C','D','E','F','G','H','I','J','K']
    lista_salida = []
    for elemento in lista_entrada:
        lista_salida.append(elemento + ':' + str(lista_entrada.index(elemento)+1) + '\n') #barra invertida hace que sea un caracter especial y que no se escriba tal cual \n sino que pase de linea
        
    with open('salida-ej3.txt','w') as descriptor_fichero:
        descriptor_fichero.writelines(lista_salida)
        
    