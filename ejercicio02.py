def mayor(num1, num2):
    resultado = num1
    if num2 > num1:
        resultado = num2
    return resultado

#(********)
#(* main *)
#(********)

if __name__ == "__main__":
    lista = [7,4,5,6]
    print(mayor(lista[0], lista[1]))
    print(mayor(lista[2], lista[3]))
    print(mayor(mayor(lista[0], lista[1]), mayor(lista[2], lista[3])))